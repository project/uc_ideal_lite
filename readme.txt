iDeal payment module for Ubercart

PREREQUISITES

- Drupal 5.X

INSTALLATION

Install and activate this module like every other Drupal
module.

DESCRIPTION

Receive payments through checkout via Ideal ING/Postbank lite or Rabobank basic.

AUTHOR

C. Kodde
Qrios Webdiensten
http://qrios.nl
c.kodde {at} qrios {dot} nl

SPONSOR


